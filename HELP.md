# Grupo de estudos de Vuejs da Comunidade DevOpsPBS

## Criar um ambiente de desenvolvimento Vuejs com Docker

Com o git instalado e devidamente configurado rodar:

```console
git clone https://gitlab.com/devopspbs/front-end/vue-docker-compose.git
cd vue-docker-compose
```

Com docker e docker-compose devidamente instalados e configurados. Criar um novo projeto:

```console
docker-compose run vuejs-app vue create -d .
```

Em seguida inicie o ambiente:
```console
docker-compose up
```


Se você estiver rodando Docker diretamente sobre Linux é necessário ajustar as permissões. Lembre-se de repetir o comando abaixo sempre que estiver problemas para acessar os arquivos diretamente no Docker host.

```console
sudo chown -R $USER:$USER .
```

## Referências


## License

GPLv3 or later.
