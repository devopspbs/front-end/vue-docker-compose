FROM node:lts-buster

# Instala o vue cli.
RUN npm install -g @vue/cli

# Cria o diretorio 'app'.
RUN mkdir /app

# Faz da pasta 'app' o diretório de trabalho corrente.
WORKDIR /app

# Exponhe a porta 8080 para ser acessada de fora do ambiente
# do docker container.
EXPOSE 8080